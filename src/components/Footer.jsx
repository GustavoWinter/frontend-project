//Libs
import React, { Component } from 'react';

// Svg
import Gifthumbnail from './Gifthumbnail'


export default class Footer extends Component {

  constructor() {
    super();
    this.state = {
      initGif: 0,
      endGif: 5
    }

    this.nextGif = this.nextGif.bind(this);
    this.prevGif = this.prevGif.bind(this);
  }

//Set the five next gifs to appear

  nextGif() {
    if(this.state.endGif>=25){
      this.setState(state => ({
        initGif: 20,
        endGif: 25
      }));
    } else {
      this.setState(state => ({
        initGif: (this.state.initGif + 5),
        endGif: (this.state.endGif + 5)
      }));
    }
  }

//Set the previous five gifts to appear

  prevGif() {
    if(this.state.initGif<=0){
      this.setState(state => ({
        initGif: 0,
        endGif: 5
      }));
    } else {
      this.setState(state => ({
        initGif: (this.state.initGif - 5),
        endGif: (this.state.endGif - 5)
      }));
    }
  }

// Show the gifts
  showGifs(initGif = this.state.initGif, endGif = this.state.endGif) {
    const results = this.props.data;
    let gifs;
    let gifSlice = results.slice(initGif, endGif);
    gifs = gifSlice.map(gif =>

      <Gifthumbnail url={gif.images.fixed_height.url} key={gif.id} liKey={gif.id} />
    )
    return gifs
  }




  render() {
    const showGifs = this.showGifs();

    return(
      <div className="footer">
         <button className="svgArrowBack" onClick={this.prevGif} ></button>
         <ul className="thumbnailList" onClick={this.props.handleChoice}>
            {showGifs}
         </ul>
         <button className="svgArrowNext" onClick={this.nextGif} >
        </button>
       </div>
    );
  };
}
