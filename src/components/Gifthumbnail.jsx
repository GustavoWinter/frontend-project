// Libs
import React from 'react';

const Gifthumbnail = props => (
  <li className="gif-li-thumbnail">
    <img className="gif-img-thumbnail" src={props.url} alt="" id={props.liKey} />
  </li>
);

export default Gifthumbnail;
