// Libs
import React from 'react';

const Gif = props => (
  <li className="gif-li">
    <img className="gif-size" src={props.url} alt=""/>
  </li>
);

export default Gif;
