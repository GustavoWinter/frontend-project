//Libs
import React from 'react';

//Components
import Gif from './Gif';
import NoGifs from './NoGifs';
import EntryHome from'./EntryHome';




const Body = props =>{

//Props ass Var

  const togglePortal = props.togglePortal;
  const results = props.data;
  const initialGif = props.initialGif;

  let gifs;

// Handle the upcoming Gif states

  if(initialGif.length) {
    if(results.length){
      gifs = results.map(gif => {
        if(gif.id === initialGif) {
          return <Gif url={gif.images.fixed_height.url} key={gif.id} />
        }
        return null
      })
    } else {
      // If no gif's match the result. Show the NoGifs
      gifs = <NoGifs />
    }
  } else {
    //If there is no gifs at the start point. Show the EntryHome
    gifs = <EntryHome />
  }





  return (
    <div className="body">
      <div className="bodyVideo">
        {gifs}
      </div>
      <div className="bodyButton">
        <button
            type="button"
            id="submit"
            className="thumbnailButton"
            onClick={togglePortal}
             ></button>
      </div>
    </div>
  );
}



export default Body;
