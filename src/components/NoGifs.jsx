// Libs
import React from 'react';

const NoGifs = props => (
  <li className='no-gifs'>
    <i className="material-icons icon-gif">Sorry, we do something WRONG.</i>
    <h3>Sorry, no GIFs match your search.</h3>
  </li>
);

export default NoGifs;
