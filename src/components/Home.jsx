//Libs
import React, { Component } from 'react';

// Components
import Body from './Body';
import Footer from './Footer';
import SearchForm from './SearchForm';
import Modal from './Modal';

export default class Home extends Component {

  constructor() {
    super();
    this.state = {
      gifs: [],
      showWindowPortal: false,
      initialGif: []
    };

    this.toggleWindowPortal = this.toggleWindowPortal.bind(this);
    this.handleChoice = this.handleChoice.bind(this)
  }

  componentDidMount() {
    this.performSearch();
  }

//Control the Moal Portal

  toggleWindowPortal() {
    this.setState(state => ({
      ...state,
      showWindowPortal: !state.showWindowPortal,
    }));
  }

// Get the id from the gifs options trought the Thumbnail

  handleChoice(e) {
    let newInitGif;
    const results = this.state.gifs;
    results.map(gifs => {
      if(gifs.id === e.target.id){
        newInitGif = gifs.id
      }
      return null
    });
    this.setState(({
      initialGif: newInitGif
    }))
  }



  // Get the Gifs from the Giphy api

  performSearch = (query = 'dogs') => {
    fetch(`http://api.giphy.com/v1/gifs/search?q=${query}&limit=25&api_key=S1E19oSH3IttBWotIJRXobWRY46E2Ra1`)
      .then(response => response.json())
      .then(responseData => {
        this.setState({gifs: responseData.data});
      })
      .catch(error => {
        console.log('Error fetching and parsing data', error);
      });
  }

  render() {
    return (
      <div className="home">
        <div className="header">
          <h1 >Channel Name</h1>
          <SearchForm onSearch={this.performSearch} />
        </div>
        <Body data={this.state.gifs} togglePortal={this.toggleWindowPortal} initialGif={this.state.initialGif}/>
        {this.state.showWindowPortal && (
          <Modal>
            <div className="modal">
              <Footer data={this.state.gifs} handleChoice={this.handleChoice} />
            </div>
          </Modal>
        )}
      </div>
    );
  }
};
