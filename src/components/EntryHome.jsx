// Libs
import React from 'react';

const EntryHome = props => (
  <div>
    <h1> Please, use the input to search for the availables titles</h1>
    <h2> And see the options in the Thumbnail button</h2>
  </div>
);

export default EntryHome;
