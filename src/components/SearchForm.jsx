//Libs
import React, { Component } from 'react';

//Svg Icon
import Loupe from '../assets/icons/loupe.svg'

export default class SearchForm extends Component {

  state = {
    searchText: ''
  }

//Set the current value to the searchField

  onSearchChange = e => {
    this.setState({ searchText: e.target.value });
  }


//Handle the submit searchField

  handleSubmit = e => {
    e.preventDefault();
    this.props.onSearch(this.query.value);
    e.currentTarget.reset();
  }

  render() {
    return (
      <form className="searchForm" onSubmit={this.handleSubmit} >
        <input type="search"
               onChange={this.onSearchChange}
               name="search"
               ref={(input) => this.query = input}
               placeholder="Search..." />
        <button type="submit" id="submit" disabled={!this.state.searchText}><img src={Loupe} alt="loupe" className="loupeSVG"/></button>
      </form>
    );
  }
}
