//Libs
import React, { Component } from 'react';

// Components
import Home from './components/Home';

// Styles
import './App.css';

class App extends Component {


  render() {
    return (
      <div className="MainContainer">
        <Home />
      </div>
    );
  }
}

export default App;
